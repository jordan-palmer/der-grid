#!bin/bash
source /scratch4/jpalmer/UKDC/dirac_ui/bashrc
dirac-dms-find-lfns Path=/lz/user/j/jordan.palmer/photonCountMCTruthHigherE/*mctruth.root>outputfile.txt

while read p; do
    echo "Processing $p file..."
    echo $p > input.txt
    dirac-wms-job-submit myjob.jdl 
done<outputfile.txt

