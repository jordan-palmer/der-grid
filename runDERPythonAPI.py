# setup DIRAC
from DIRAC.Core.Base import Script
Script.parseCommandLine(ignoreErrors=False)
from DIRAC.Interfaces.API.Job import Job
from DIRAC.Interfaces.API.Dirac import Dirac
import subprocess
import os

os.system('dirac-dms-find-lfns Path=/lz/user/j/jordan.palmer/Kr83mmctruth_run2/*mctruth.root>outputfile.txt')

f = open('outputfile.txt')
dirac = Dirac()
for line in f:
    j = Job()
    print("Processing " + line)
    with open('input.txt', 'w') as the_file:
        the_file.write(line)
    j.setInputSandbox(['input.txt','DER.sh'])
    j.setExecutable('DER.sh')
    j.setPlatform('EL7')
    j.setOutputSandbox(['job.log'])
    j.setOutputData(['*.root'],outputSE = 'UKI-LT2-IC-HEP-disk')
    jobID = dirac.submitJob(j)
    print 'Submission Result: ',jobID
    break
